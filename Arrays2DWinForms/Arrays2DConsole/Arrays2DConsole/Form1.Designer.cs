﻿namespace Arrays2DConsole
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Kilkist_row = new System.Windows.Forms.Label();
            this.SumaElementiv = new System.Windows.Forms.Label();
            this.SumaElementsArray = new System.Windows.Forms.TextBox();
            this.numericUpDownRow = new System.Windows.Forms.NumericUpDown();
            this.Generator = new System.Windows.Forms.Button();
            this.Solving = new System.Windows.Forms.Button();
            this.Kilkist_column = new System.Windows.Forms.Label();
            this.numericUpDownColumn = new System.Windows.Forms.NumericUpDown();
            this.dataGridViewMatrix = new System.Windows.Forms.DataGridView();
            this.NameAndSurnameOfStudent = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).BeginInit();
            this.SuspendLayout();
            // 
            // Kilkist_row
            // 
            this.Kilkist_row.AutoSize = true;
            this.Kilkist_row.BackColor = System.Drawing.Color.Transparent;
            this.Kilkist_row.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Kilkist_row.ForeColor = System.Drawing.Color.White;
            this.Kilkist_row.Location = new System.Drawing.Point(12, 7);
            this.Kilkist_row.Name = "Kilkist_row";
            this.Kilkist_row.Size = new System.Drawing.Size(101, 14);
            this.Kilkist_row.TabIndex = 0;
            this.Kilkist_row.Text = "Кількість рядків: ";
            // 
            // SumaElementiv
            // 
            this.SumaElementiv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SumaElementiv.AutoSize = true;
            this.SumaElementiv.BackColor = System.Drawing.Color.Transparent;
            this.SumaElementiv.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.SumaElementiv.ForeColor = System.Drawing.Color.White;
            this.SumaElementiv.Location = new System.Drawing.Point(12, 339);
            this.SumaElementiv.Name = "SumaElementiv";
            this.SumaElementiv.Size = new System.Drawing.Size(172, 14);
            this.SumaElementiv.TabIndex = 1;
            this.SumaElementiv.Text = "Сума усіх елементів матриці: ";
            // 
            // SumaElementsArray
            // 
            this.SumaElementsArray.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SumaElementsArray.Location = new System.Drawing.Point(209, 336);
            this.SumaElementsArray.Name = "SumaElementsArray";
            this.SumaElementsArray.ReadOnly = true;
            this.SumaElementsArray.Size = new System.Drawing.Size(90, 20);
            this.SumaElementsArray.TabIndex = 2;
            // 
            // numericUpDownRow
            // 
            this.numericUpDownRow.Location = new System.Drawing.Point(120, 5);
            this.numericUpDownRow.Name = "numericUpDownRow";
            this.numericUpDownRow.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownRow.TabIndex = 3;
            this.numericUpDownRow.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Generator
            // 
            this.Generator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Generator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Generator.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Generator.Location = new System.Drawing.Point(237, 22);
            this.Generator.Name = "Generator";
            this.Generator.Size = new System.Drawing.Size(90, 23);
            this.Generator.TabIndex = 4;
            this.Generator.Text = "Генерувати";
            this.Generator.UseVisualStyleBackColor = true;
            this.Generator.Click += new System.EventHandler(this.Generator_Click);
            // 
            // Solving
            // 
            this.Solving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Solving.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Solving.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Solving.Location = new System.Drawing.Point(382, 22);
            this.Solving.Name = "Solving";
            this.Solving.Size = new System.Drawing.Size(90, 23);
            this.Solving.TabIndex = 5;
            this.Solving.Text = "Розв\'язати";
            this.Solving.UseVisualStyleBackColor = true;
            this.Solving.Click += new System.EventHandler(this.Solving_Click);
            // 
            // Kilkist_column
            // 
            this.Kilkist_column.AutoSize = true;
            this.Kilkist_column.BackColor = System.Drawing.Color.Transparent;
            this.Kilkist_column.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Kilkist_column.ForeColor = System.Drawing.Color.White;
            this.Kilkist_column.Location = new System.Drawing.Point(12, 43);
            this.Kilkist_column.Name = "Kilkist_column";
            this.Kilkist_column.Size = new System.Drawing.Size(111, 14);
            this.Kilkist_column.TabIndex = 6;
            this.Kilkist_column.Text = "Кількість стовпців:";
            // 
            // numericUpDownColumn
            // 
            this.numericUpDownColumn.Location = new System.Drawing.Point(120, 41);
            this.numericUpDownColumn.Name = "numericUpDownColumn";
            this.numericUpDownColumn.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownColumn.TabIndex = 7;
            this.numericUpDownColumn.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // dataGridViewMatrix
            // 
            this.dataGridViewMatrix.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewMatrix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewMatrix.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewMatrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMatrix.Location = new System.Drawing.Point(15, 67);
            this.dataGridViewMatrix.Name = "dataGridViewMatrix";
            this.dataGridViewMatrix.Size = new System.Drawing.Size(457, 255);
            this.dataGridViewMatrix.TabIndex = 8;
            this.dataGridViewMatrix.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewMatrix_CellPainting);
            // 
            // NameAndSurnameOfStudent
            // 
            this.NameAndSurnameOfStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NameAndSurnameOfStudent.AutoSize = true;
            this.NameAndSurnameOfStudent.BackColor = System.Drawing.Color.Transparent;
            this.NameAndSurnameOfStudent.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameAndSurnameOfStudent.ForeColor = System.Drawing.Color.White;
            this.NameAndSurnameOfStudent.Location = new System.Drawing.Point(332, 339);
            this.NameAndSurnameOfStudent.Name = "NameAndSurnameOfStudent";
            this.NameAndSurnameOfStudent.Size = new System.Drawing.Size(145, 14);
            this.NameAndSurnameOfStudent.TabIndex = 9;
            this.NameAndSurnameOfStudent.Text = "Ніколайчук Р.О., гр. СН-21";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(484, 361);
            this.Controls.Add(this.NameAndSurnameOfStudent);
            this.Controls.Add(this.dataGridViewMatrix);
            this.Controls.Add(this.numericUpDownColumn);
            this.Controls.Add(this.Kilkist_column);
            this.Controls.Add(this.Solving);
            this.Controls.Add(this.Generator);
            this.Controls.Add(this.numericUpDownRow);
            this.Controls.Add(this.SumaElementsArray);
            this.Controls.Add(this.SumaElementiv);
            this.Controls.Add(this.Kilkist_row);
            this.MaximumSize = new System.Drawing.Size(550, 450);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №3. Завдання 2";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Kilkist_row;
        private System.Windows.Forms.Label SumaElementiv;
        private System.Windows.Forms.TextBox SumaElementsArray;
        private System.Windows.Forms.NumericUpDown numericUpDownRow;
        private System.Windows.Forms.Button Generator;
        private System.Windows.Forms.Button Solving;
        private System.Windows.Forms.Label Kilkist_column;
        private System.Windows.Forms.NumericUpDown numericUpDownColumn;
        private System.Windows.Forms.DataGridView dataGridViewMatrix;
        private System.Windows.Forms.Label NameAndSurnameOfStudent;
    }
}

