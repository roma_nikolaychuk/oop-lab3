﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DConsole
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double[,] arr;
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Generator_Click(object sender, EventArgs e)
        {
            int KilkistElementivRow = (int)numericUpDownRow.Value;
            int KilkistElementivColumn = (int)numericUpDownColumn.Value;
            arr = new double[KilkistElementivRow, KilkistElementivColumn];

            Random rnd = new Random();
            for (int i = 0; i < KilkistElementivRow; i++)
            {
 
                for (int j = 0; j < KilkistElementivColumn; j++)
                {
                    dataGridViewMatrix.RowCount = KilkistElementivRow;
                    dataGridViewMatrix.ColumnCount = KilkistElementivColumn;
                    arr[i,j] = rnd.Next(-15000, 20000) / 100.0;
                    
                    dataGridViewMatrix[j, i].Value = arr[i, j];
                }

            }
            for (int i = 0; i < arr.GetLength(1); i++)
            {
                dataGridViewMatrix.Columns[i].HeaderText = i.ToString();
                dataGridViewMatrix.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            for (int j = 0; j < arr.GetLength(0); j++)
                dataGridViewMatrix.Rows[j].HeaderCell.Value = j.ToString();
        }

        private void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == -1 && e.RowIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black))
                {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(e.RowIndex.ToString(),
                    e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }

        private void Solving_Click(object sender, EventArgs e)
        {
            double sum = 0.00;
            int KilkistElementivRow = (int)numericUpDownRow.Value;
            int KilkistElementivColums = (int)numericUpDownColumn.Value;

            for (int i = 0; i < KilkistElementivRow; i++)
                for (int j = 0; j < KilkistElementivColums; j++)
                    dataGridViewMatrix[j, i].Value = arr[i, j];
            for (int i = 0; i < KilkistElementivRow; i++)
                for(int j = 0; j < KilkistElementivColums; j++)
                    sum += arr[i, j];
            SumaElementsArray.Text = sum.ToString();

        }
    }
}
