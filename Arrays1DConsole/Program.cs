﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1DConsole
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            
            int n;
            Console.Write("Введіть кількість елементів масиву: ");
            
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            double[] array = new double[n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                array[i] = rand.Next(-12, 16) + rand.NextDouble();
            }
            
            for(int i = 0; i < n; i++)
            {
                double dn = Math.Round(array[i], 1);
                Console.WriteLine(dn);
            }
            // Сума індексів від'ємних елементів, які мають індекси, які діляться на 2
            double Suma = 0;
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {

                if (array[i] < 0)
                {
                    Suma += (i/2);
                }
                
            }
            Console.WriteLine($"Сума індексів від'ємних елементів, які діляться на 2: {Convert.ToDouble(Suma)}");
            Console.WriteLine();
            int kilkist = 0;
            int one_num = 0;
            int two_num = 0;
            for (int i = 0; i < n; i++)
            {
                if(array[i] > 0)
                {
                    kilkist++;

                  if (kilkist == 1)
                     {
                        one_num = i;
                     }
                  if (kilkist == 2)
                     {
                        two_num = i;
                     }          
                }   
            }
            Console.WriteLine();
            Console.WriteLine(one_num + " - індекс першого додатного елемента.");
            Console.WriteLine(two_num + " - індекс другого додатного елемента.");
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                Array.Sort(array, two_num, one_num);
                Console.WriteLine(Math.Round(array[i], 1));  
            }
            Console.WriteLine();
            Console.WriteLine("Кількість = " + kilkist);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
