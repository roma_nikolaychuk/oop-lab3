﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays2DConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            int n;
            int m;

            do
            {
                Console.Write("Введіть кількість рядків матриці n = ");
                if (!int.TryParse(Console.ReadLine(), out n) || n <= 0)
                    Console.WriteLine("Помилка введеня значення n! Будь ласка повторіть введене значення ще раз!");
                else
                    break;
            } while (true);

            do
            {
                Console.Write("Введіть кількість стовпців матриці m = ");
                if (!int.TryParse(Console.ReadLine(), out m) || m <= 0)
                    Console.WriteLine("Помилка введеня значення m! Будь ласка повторіть введене значення ще раз!");
                else
                    break;

            } while (true);

            double[,] array = new double[n, m];
            Random rand = new Random();
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = rand.Next(-14, 18) + rand.NextDouble();
                    Console.Write("{0}\t", Math.Round(array[i, j], 1));
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            double Suma = 0;
            for (int i = 0; i < n; i++)
            { 
                for (int j = 0; j < m; j++)
                {
                    Suma += array[i, j];
                }             
                Console.WriteLine($"Сума елементів {i} рядка = {Math.Round(Suma, 1)}");                           
                Suma = 0;
            }
            Console.WriteLine();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m / 2; j++)
                {
                    double x = array[i, j];
                    array[i, j] = array[i, (m - 1) - j];
                    array[i, (m - 1) - j] = x;
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                    Console.Write(Math.Round(array[i, j], 1) + "\t");
                    Console.WriteLine();   
            }
            Console.ReadKey();
        }
    }
}
