﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays1DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        double[] arr;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void generate_Click(object sender, EventArgs e)
        {
           int kilkist_elementiv = (int)numericUpDownCount.Value;
           arr = new double[kilkist_elementiv];
            
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                dataGridViewArray.RowCount = 1;
                dataGridViewArray.ColumnCount = kilkist_elementiv;
                arr[i] = rnd.Next(-15000, 20000) / 100.0;
                dataGridViewArray[i, 0].Value = arr[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();
            }
        }
        
        private void solve_Click(object sender, EventArgs e)
        {
            double sum = 0;
            int kilkist_elementiv = (int)numericUpDownCount.Value;
            
            for (int i = 0; i < arr.Length; i++)
                dataGridViewArray[i, 0].Value = arr[i];
            for (int i = 0; i < arr.Length; i++)
                if (arr[i] > 0)
                    sum += arr[i];
            Text_Box_Suma_elemArray.Text = sum.ToString();
        }
    }
}
