﻿namespace Arrays1DWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kilkist_element = new System.Windows.Forms.Label();
            this.numericUpDownCount = new System.Windows.Forms.NumericUpDown();
            this.generate = new System.Windows.Forms.Button();
            this.solve = new System.Windows.Forms.Button();
            this.dataGridViewArray = new System.Windows.Forms.DataGridView();
            this.label_Suma_elemArray = new System.Windows.Forms.Label();
            this.Text_Box_Suma_elemArray = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).BeginInit();
            this.SuspendLayout();
            // 
            // kilkist_element
            // 
            this.kilkist_element.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kilkist_element.AutoSize = true;
            this.kilkist_element.BackColor = System.Drawing.Color.Transparent;
            this.kilkist_element.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kilkist_element.ForeColor = System.Drawing.Color.White;
            this.kilkist_element.Location = new System.Drawing.Point(14, 14);
            this.kilkist_element.Name = "kilkist_element";
            this.kilkist_element.Size = new System.Drawing.Size(141, 16);
            this.kilkist_element.TabIndex = 0;
            this.kilkist_element.Text = "Кількість елементів: ";
            // 
            // numericUpDownCount
            // 
            this.numericUpDownCount.Location = new System.Drawing.Point(160, 12);
            this.numericUpDownCount.MinimumSize = new System.Drawing.Size(120, 0);
            this.numericUpDownCount.Name = "numericUpDownCount";
            this.numericUpDownCount.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownCount.TabIndex = 1;
            this.numericUpDownCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // generate
            // 
            this.generate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.generate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generate.Location = new System.Drawing.Point(300, 12);
            this.generate.MaximumSize = new System.Drawing.Size(100, 25);
            this.generate.Name = "generate";
            this.generate.Size = new System.Drawing.Size(100, 25);
            this.generate.TabIndex = 2;
            this.generate.Text = "Генерувати";
            this.generate.UseVisualStyleBackColor = true;
            this.generate.Click += new System.EventHandler(this.generate_Click);
            // 
            // solve
            // 
            this.solve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.solve.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.solve.Location = new System.Drawing.Point(475, 12);
            this.solve.MaximumSize = new System.Drawing.Size(100, 25);
            this.solve.Name = "solve";
            this.solve.Size = new System.Drawing.Size(100, 25);
            this.solve.TabIndex = 3;
            this.solve.Text = "Розв\'язати";
            this.solve.UseVisualStyleBackColor = true;
            this.solve.Click += new System.EventHandler(this.solve_Click);
            // 
            // dataGridViewArray
            // 
            this.dataGridViewArray.AllowUserToAddRows = false;
            this.dataGridViewArray.AllowUserToDeleteRows = false;
            this.dataGridViewArray.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArray.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArray.Location = new System.Drawing.Point(14, 49);
            this.dataGridViewArray.Name = "dataGridViewArray";
            this.dataGridViewArray.ReadOnly = true;
            this.dataGridViewArray.Size = new System.Drawing.Size(560, 264);
            this.dataGridViewArray.TabIndex = 4;
            // 
            // label_Suma_elemArray
            // 
            this.label_Suma_elemArray.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Suma_elemArray.AutoSize = true;
            this.label_Suma_elemArray.BackColor = System.Drawing.Color.Transparent;
            this.label_Suma_elemArray.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Suma_elemArray.ForeColor = System.Drawing.Color.White;
            this.label_Suma_elemArray.Location = new System.Drawing.Point(11, 336);
            this.label_Suma_elemArray.Name = "label_Suma_elemArray";
            this.label_Suma_elemArray.Size = new System.Drawing.Size(233, 16);
            this.label_Suma_elemArray.TabIndex = 5;
            this.label_Suma_elemArray.Text = "Сума додатних елементів масиву: ";
            // 
            // Text_Box_Suma_elemArray
            // 
            this.Text_Box_Suma_elemArray.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_Box_Suma_elemArray.Location = new System.Drawing.Point(239, 335);
            this.Text_Box_Suma_elemArray.Name = "Text_Box_Suma_elemArray";
            this.Text_Box_Suma_elemArray.Size = new System.Drawing.Size(100, 20);
            this.Text_Box_Suma_elemArray.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(379, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ніколайчук Р.О., група СН-21";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Arrays1DWinForms.Properties.Resources.aluminum_texture1784;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Text_Box_Suma_elemArray);
            this.Controls.Add(this.label_Suma_elemArray);
            this.Controls.Add(this.dataGridViewArray);
            this.Controls.Add(this.solve);
            this.Controls.Add(this.generate);
            this.Controls.Add(this.numericUpDownCount);
            this.Controls.Add(this.kilkist_element);
            this.MaximumSize = new System.Drawing.Size(700, 500);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №3. Завдання 1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label kilkist_element;
        private System.Windows.Forms.NumericUpDown numericUpDownCount;
        private System.Windows.Forms.Button generate;
        private System.Windows.Forms.Button solve;
        private System.Windows.Forms.DataGridView dataGridViewArray;
        private System.Windows.Forms.Label label_Suma_elemArray;
        private System.Windows.Forms.TextBox Text_Box_Suma_elemArray;
        private System.Windows.Forms.Label label1;
    }
}

